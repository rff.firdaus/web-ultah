import Vue from "vue";
import App from "./App.vue";
import "bootstrap/dist/css/bootstrap.css";
import "@/assets/scss/style.scss";
import VueTilt from "vue-tilt.js";
Vue.use(VueTilt);
Vue.config.productionTip = false;

new Vue({
  render: (h) => h(App),
}).$mount("#app");
